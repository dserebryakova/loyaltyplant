package com.loyaltyplant;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LoyaltyPlantApplication {

    public static void main(String[] args) {
        SpringApplication.run(LoyaltyPlantApplication.class, args);
    }

}
