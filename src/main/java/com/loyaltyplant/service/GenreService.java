package com.loyaltyplant.service;

import com.loyaltyplant.model.Genre;
import java.util.List;

public interface GenreService {
    List<Genre> getAllGenres();
}
