package com.loyaltyplant.service;

import com.loyaltyplant.model.Movie;
import com.loyaltyplant.model.TemporaryResult;

import java.util.List;

public interface CalculationService {
    TemporaryResult calculation(List<Movie> movies, Integer genreId);
}
