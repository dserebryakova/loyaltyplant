package com.loyaltyplant.service;

import com.loyaltyplant.model.TemporaryResult;

public interface TaskService {
    TemporaryResult calculation (int genreId) throws InterruptedException;
}
