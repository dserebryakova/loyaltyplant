package com.loyaltyplant.service;

import com.loyaltyplant.model.TemporaryResult;

public interface ExecutorTaskMapWrapperService {

    TemporaryResult startTask(int id);
    TemporaryResult getInfo(int genreId);
    void stop(int genreId);
}
